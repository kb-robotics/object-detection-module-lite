import yaml
import sys
import os
import torch
import shutil
import cv2
import torchvision.transforms as transforms_test
from torchvision.ops import nms

from utility_scripts.engine import evaluate

# prediction function
def predict(image, model, device, detection_threshold, nms_threshold, class_mapping):
    # transform the image to tensor
    image = image.to(device)
    image = image.unsqueeze(0) # add a batch dimension
    outputs = model(image) # get the predictions on the image
    # get all the predicited class names
    pred_classes = outputs[0]['labels']
    # get score for all the predicted objects
    pred_scores = outputs[0]['scores']
    # get all the predicted bounding boxes
    pred_bboxes = outputs[0]['boxes']
    # get boxes and classes above the threshold score
    boxes = pred_bboxes[pred_scores >= detection_threshold]
    scores = pred_scores[pred_scores >= detection_threshold]
    classes = pred_classes[pred_scores >= detection_threshold]
    # non-maximum suppression (NMS) on the boxes according to their intersection-over-union (IoU)
    keep_idx = nms(boxes, scores, nms_threshold) 
    boxes_nms = boxes[keep_idx].cpu().detach().numpy().astype('int32')
    scores_nms = scores[keep_idx].cpu().detach().numpy()
    classes_nms = classes[keep_idx].cpu().detach().numpy()
    # convert class back to string
    classes_nms = [class_mapping[i] for i in classes_nms]
    return boxes_nms, classes_nms


if __name__ == "__main__":

    # load configfile
    with open(r'conf.yaml') as conf_file:
        conf_dict = yaml.load(conf_file, Loader=yaml.FullLoader)

    eval_model = conf_dict["eval_model"]
    test_video_dir = conf_dict["test_video_dir"]
    size = tuple(conf_dict["size"])
    threshold = conf_dict["threshold"]
    nms_threshold = conf_dict["nms_threshold"]
    evaluate_classes = conf_dict["evaluate_classes"]

    # load and check gpu
    device = torch.device("cuda")

    # check if GPU is loaded
    if torch.cuda.is_available():
        print("GPU loaded, continu training ...")
    else:
        sys.exit("GPU not loading with Cuda, please make sure Cuda is available")

    # load model
    model = torch.load(eval_model)

    # evaluate model
    #evaluate(model, test, device=device)
    model.eval()

    # list root, directories and files in video_test folder
    walk = list(os.walk(test_video_dir))[0]
    root = walk[0]
    dirs = walk[1]
    files = walk[2]

    # delete all previous annotations
    for directory in dirs:
        shutil.rmtree(root + "/" + directory)

    # delete all previous annotated videos
    for file in files:
        if file.endswith('.avi'):
            os.remove(root + "/" + file)
    
    # get names of all test_videos
    test_videos = [i for i in files if i.endswith('.mp4')]

    # class mapping
    evaluate_classes.sort
    # the label mapping to integers needs to start from 1 because 0 is the index for the background
    index = range(1, len(evaluate_classes)+1)
    class_mapping = dict(zip(index, evaluate_classes))
    
    # make a color mapping for 1 to 3 class scenarios
    color_map = {}
    for idx, scenario_class in enumerate(evaluate_classes):
        color = [0,0,0]
        color[idx] = 255
        color_map[scenario_class] = tuple(color)

    for test_video in test_videos:
        count = 0
        # get video
        cap = cv2.VideoCapture(test_video_dir + "/" + test_video)
        # make folder for video annotations
        foldername = test_video_dir + "/" + test_video.split(".mp4")[0]
        os.mkdir(foldername)
        # create output video framework
        out = cv2.VideoWriter(foldername + '.avi', cv2.VideoWriter_fourcc(*'XVID'), 20.0, size)

        while(cap.isOpened()):
            ret, frame = cap.read()
            if (ret != True):
                break
                # frame is RGB and should be BGR
            frame_tensor = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
                # generate file file for annotated frame
            filename = foldername + "/" + "frame_%d.jpg" % count;count+=1
                # transform to tensor for predictions
            frame_tensor = transforms_test.ToTensor()(frame_tensor)
                # make predictions
            bboxes, labels = predict(frame_tensor, model, device, threshold, nms_threshold, class_mapping)
            # put in legenda
            cv2.rectangle(frame,(0,0),(180,100),(255,255,255),-1)
            position_index = 1
            for scenario_class, color in color_map.items():
                cv2.putText(frame,scenario_class,(0,30*position_index),0,1,color)
                position_index += 1
            # put bounding box and label in frame
            for i in range(len(bboxes)):
                cv2.rectangle(frame,(bboxes[i][0],bboxes[i][1]),(bboxes[i][2],bboxes[i][3]),color_map[labels[i]],2)
            # save frame as image
            cv2.imwrite(filename, frame)
            # write frame to output video
            out.write(frame)

            # get size of frame
            height, width, layers = frame.shape
            size = (width,height)
        print("size of input video: {}".format(size))
        
        cap.release()
        out.release()
