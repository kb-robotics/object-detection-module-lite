import pandas as pd
import os
from xml.dom import minidom
from torch.utils.data import Dataset
import torch
from PIL import Image
import numpy as np
import torchvision.transforms as transforms_test
import json
import requests


# create function for converting string to integer according to class mapping
def convert_class_string_to_int(class_mapping, label):
    return list(class_mapping.keys())[list(class_mapping.values()).index(label)]


def imagenet_dataframe(imagenet_image_dir, imagenet_annotation_dir, imagenet_classes=None):
    # if classes are not hard coded, find all classes available
    if imagenet_classes == None:
        imagenet_classes = os.listdir(imagenet_image_dir)
    # create a df in pandas for imagent dataset
    imagenet_df = pd.DataFrame(columns = ["path", "labels", "boxes"])
    # we hardcode the classes because we want toxclude advocado
    # Tracking the total number of annotated objects across all images
    imagenet_labels = []
    # Counting the total number of images in dataset
    imagenet_images = 0
    # loop over classes
    for item in imagenet_classes:
        # create path to image directory for the given class
        image_path = imagenet_image_dir +"/"+ item
        # create path to annotation directory for the given class
        annotation_path = imagenet_annotation_dir +"/"+ item
        # loop over all images in image directory of a given class
        for item_id in os.listdir(image_path):
            # we need to do a try statement because not all images have annotations
            try:
                xml = minidom.parse(annotation_path +"/"+ item_id.split(".jpg")[0] + '.xml')
            except:
                continue
            # get labels through xml tag name
            labels_xml = xml.getElementsByTagName('name')
            # we are catching all labels in a image
            labels = []
            for label_xml in labels_xml:
                label = label_xml.firstChild.data
                # catch weird labeling errors
                if label == 'cucumber\ncuke':
                    labels.append('cucumber')
                elif label == 'avocado\nalligator pear\navocado pear\naguacate':
                    labels.append('avocado')
                # covert name of bell pepper to bell_pepper
                elif label == 'bell pepper':
                    labels.append('bell_pepper')
                # label sweet oranges as oranges
                elif label == 'sweet orange':
                    labels.append('orange')
                # label granny smiths as apples
                elif label == 'Granny Smith':
                    labels.append('apple')
                # if nothing weird is going on, add the label as it is
                else:
                    labels.append(label)
            # Tracking the total number of annotated objects across all images
            imagenet_labels += labels
            # Getting bbox data through xml tag name
            xmins_xml = xml.getElementsByTagName('xmin')
            xmins = [xmin_xml.childNodes[0].data for xmin_xml in xmins_xml]
            ymins_xml = xml.getElementsByTagName('ymin')
            ymins = [ymin_xml.childNodes[0].data for ymin_xml in ymins_xml]
            xmaxs_xml = xml.getElementsByTagName('xmax')
            xmaxs = [xmax_xml.childNodes[0].data for xmax_xml in xmaxs_xml]
            ymaxs_xml = xml.getElementsByTagName('ymax')
            ymaxs = [ymax_xml.childNodes[0].data for ymax_xml in ymaxs_xml]
            # combine bbox data
            boxes = [list(a) for a in zip(xmins, ymins, xmaxs, ymaxs)]
            # Counting the total number of images in dataset
            imagenet_images += 1
            # add data to pandas dataframe
            imagenet_df = imagenet_df.append({"path":image_path + "/"+ item_id, "labels":labels, "boxes":boxes},ignore_index = True)
            
    return imagenet_df, imagenet_classes, imagenet_labels, imagenet_images


def open_image_dataframe(open_image_dir, classes=None):
    # create a df in pandas
    df = pd.DataFrame(columns = ["path", "labels", "boxes"])
    # get classes in open_image dataset directory if classes is None
    if classes == None:
        classes = os.listdir(open_image_dir)
    # Tracking all objects over all images
    total_labels = []
    # Counting all images
    total_images = 0
    # loop over classes
    for item in classes:
        # get path to image directory of a given class
        path = open_image_dir +"/"+ item
        # loop over all images of the class
        for item_id in os.listdir(path):
            # exception because there is a folder in every class directory with labels
            if item_id == "labels":
                continue
            # Counting all images
            total_images += 1
            # fetching all labels and boxes in an image
            labels = []
            boxes = []
            # open annotation file for a given image
            with open(path + "/labels/" + item_id.split(".jpg")[0] + '.txt') as reader:
                # Every line has information about an object in the image. 
                # read all lines in annotation file and loop over every line
                lines = reader.readlines()
                for line in lines:
                    # split line in strings and catch labels and boxes
                    temp = line.split()
                    labels.append(temp[0])
                    boxes.append(temp[1:])
                    # Tracking all objects over all images
                    total_labels.append(temp[0])
            # add image information to dataframe
            df = df.append({"path":path + "/"+ item_id, "labels":labels, "boxes":boxes},ignore_index = True)
            
    return df, classes, total_labels, total_images


def scenario_dataframe(scenario_dir):
    # create a df in pandas 
    scenario_df = pd.DataFrame(columns = ["path", "labels", "boxes"])
    # Tracking the total number of annotated objects across all images
    scenario_labels = []
    # Counting the total number of images in dataset
    scenario_count_images = 0
    # loop over all annotated images in dataset
    for image_json in os.listdir(scenario_dir):
        # load image json
        with open(scenario_dir +"/"+ image_json) as json_file:
            image_data = json.load(json_file)    
        # get image path
        image_path = image_data["image"]["url"]
        # we are catching all labels and boxes in a image
        labels = []
        boxes = []
        # loop over all annotations
        for annotation in image_data["annotations"]:
            # Getting bbox data
            try:
                height = annotation["bounding_box"]["h"]
                width = annotation["bounding_box"]["w"]
                x_min = annotation["bounding_box"]["x"]
                y_min = annotation["bounding_box"]["y"]
            except:
                continue
            # we need x max and y max for our model
            x_max = x_min + width
            y_max = y_min + height
            # combine bbox data
            box = [x_min, y_min, x_max, y_max]
            boxes.append(box)
            # add one label
            label = annotation["name"]
            labels.append(label)
            # Tracking the total number of annotated objects across all images
            scenario_labels += labels
        # Counting the total number of images in dataset
        scenario_count_images += 1
        # add data to pandas dataframe
        scenario_df = scenario_df.append({"path":image_path, "labels":labels, "boxes":boxes},ignore_index = True)
    return scenario_df, scenario_labels, scenario_count_images


# pytorch dataset class for the open image dataset. Uses the panda dataframe
class DatasetOpenImage(Dataset):

    def __init__(self, pandas_data, class_mapping, transform=None):
        self.data = pandas_data
        self.class_mapping = class_mapping
        self.transform = transform
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, index):
        image_path = self.data.iloc[index, 0]
        image = Image.open(image_path).convert('RGB')

        labels = self.data.iloc[index, 1]
        boxes = np.array(self.data.iloc[index, 2], dtype=np.float32)
        
        # we need to convert labels into integers
        labels_int = [convert_class_string_to_int(self.class_mapping, x) for x in labels]
        
        # convert everything into a torch.Tensor
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.tensor(labels_int, dtype=torch.int64)
        
        # set item id to index and calculate the area of the bounding box
        image_id = torch.tensor([index])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        
        # suppose all instances are not crowd
        iscrowd = torch.zeros((len(labels),), dtype=torch.int64)
        
        # load target variable
        target = {}
        target["labels"] = labels
        target["boxes"] = boxes
        target["area"] = area
        target["image_id"] = image_id
        target["iscrowd"] = iscrowd
        
        # perform transform over image and target dict
        if self.transform is not None:
            image, target = self.transform(image, target)
            
        return image, target


# pytorch dataset class for the imagenet dataset. Uses the panda dataframe
class DatasetImagenet(Dataset):

    def __init__(self, pandas_data, class_mapping, transform=None):
        self.data = pandas_data
        self.class_mapping = class_mapping
        self.transform = transform
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, index):
        image_path = self.data.iloc[index, 0]
        image = Image.open(image_path).convert('RGB')
        
        # get width and heigth of original image
        w, h = image.size
        
        # not all images are minimal size of 1000 * 600 needed for faster RCNN model. transform to same size as open image dataset
        resizer = transforms_test.Resize((1024,683))
        image = resizer(image)
        
        # get new width and height of image
        new_w, new_h = image.size
        
        # calculate scale of width and height
        scale_w = new_w/w
        scale_h = new_h/h
        
        labels = self.data.iloc[index, 1]
        boxes_original = self.data.iloc[index, 2]
        boxes_temp = []
        for box_original in boxes_original:
            # the boxes need to be resized as well
            boxes = [int(float(box_original[0])*scale_w), int(float(box_original[1])*scale_h), int(float(box_original[2])*scale_w), int(float(box_original[3])*scale_h)]
            boxes_temp.append(boxes)
        # we need to convert labels into integers
        labels_int = [convert_class_string_to_int(self.class_mapping, x) for x in labels]
        
        # convert everything into a torch.Tensor
        boxes = torch.as_tensor(boxes_temp, dtype=torch.float32)
        labels = torch.tensor(labels_int, dtype=torch.int64)
        
        # set item id to index and calculate the area of the bounding box
        image_id = torch.tensor([index])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        
        # suppose all instances are not crowd
        iscrowd = torch.zeros((len(labels),), dtype=torch.int64)
        
        # load target variable
        target = {}
        target["labels"] = labels
        target["boxes"] = boxes
        target["area"] = area
        target["image_id"] = image_id
        target["iscrowd"] = iscrowd
        
        # transform image and target variable
        if self.transform is not None:
            image, target = self.transform(image, target)
            
        return image, target


# pytorch dataset class for the open image dataset. Uses the panda dataframe
class DatasetScenarios(Dataset):

    def __init__(self, pandas_data, class_mapping, transform=None):
        self.data = pandas_data
        self.class_mapping = class_mapping
        self.transform = transform
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, index):
        image_path = self.data.iloc[index, 0]
        image = Image.open(requests.get(image_path, stream=True).raw).convert('RGB')
        
        labels = self.data.iloc[index, 1]
        boxes = np.array(self.data.iloc[index, 2], dtype=np.float32)
        
        # we need to convert labels into integers
        labels_int = [convert_class_string_to_int(self.class_mapping, x) for x in labels]
        
        # convert everything into a torch.Tensor
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.tensor(labels_int, dtype=torch.int64)
        
        # set item id to index and calculate the area of the bounding box
        image_id = torch.tensor([index])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        
        # suppose all instances are not crowd
        iscrowd = torch.zeros((len(labels),), dtype=torch.int64)
        
        # load target variable
        target = {}
        target["labels"] = labels
        target["boxes"] = boxes
        target["area"] = area
        target["image_id"] = image_id
        target["iscrowd"] = iscrowd
        
        # perform transform over image and target dict
        if self.transform is not None:
            image, target = self.transform(image, target)
            
        return image, target
