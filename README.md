# Requirements

Tested with python: 3.8.5
GPU with CUDA

## Pip modules

*Version of module is important for scripts to work*

- **torch**==1.7.1
- **pandas**==1.2.1
- **torchvision**==0.8.2
- **pycocotools**==2.0.2

# load model

import torch

model = torch.load(<model name>)

model.eval()

# Manual

## conf.yaml

Tune settings used in script evaluate_model.py

## evaluate_model.py

Evaluate performance of model by detecting objects in video (.mp4). To run script tune conf.yaml and use command: python evaluate_model.py

_conf.yaml setting_

- **eval_model**: location of model that needs to be evaluated
- **test_video_dir**: location of videos (.mp4) directory that are going to be annoted by model
- **size**: size of output video (script will print size of input video)
- **threshold**: threshold used for making predictions (score > threshold = detection)
- **nms_threshold**: threshold used for removing boxes that overlap (https://pytorch.org/vision/stable/ops.html)
- **evaluate_classes**: list of classes that model can detect
