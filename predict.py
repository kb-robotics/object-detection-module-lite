from torchvision.ops import nms


# prediction function
def predict(image, model, device, detection_threshold, nms_threshold, class_mapping):
    # transform the image to tensor
    image = image.to(device)
    image = image.unsqueeze(0) # add a batch dimension
    outputs = model(image) # get the predictions on the image
    # get all the predicited class names
    pred_classes = outputs[0]['labels']
    # get score for all the predicted objects
    pred_scores = outputs[0]['scores']
    # get all the predicted bounding boxes
    pred_bboxes = outputs[0]['boxes']
    # get boxes and classes above the threshold score
    boxes = pred_bboxes[pred_scores >= detection_threshold]
    scores = pred_scores[pred_scores >= detection_threshold]
    classes = pred_classes[pred_scores >= detection_threshold]
    # non-maximum suppression (NMS) on the boxes according to their intersection-over-union (IoU)
    keep_idx = nms(boxes, scores, nms_threshold)
    boxes_nms = boxes[keep_idx].cpu().detach().numpy().astype('int32')
    scores_nms = scores[keep_idx].cpu().detach().numpy()
    classes_nms = classes[keep_idx].cpu().detach().numpy()
    # convert class back to string
    classes_nms = [class_mapping[i] for i in classes_nms]
    return boxes_nms, classes_nms
